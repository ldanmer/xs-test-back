<?php

use App\Http\Controllers\ServiceController;
use App\Mail\ServiceEmail;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;


class ServiceControllerTest extends TestCase
{
    private $sendData = [
        'email' => 'mail@test.com',
        'companySymbol' => 'AAIT',
        'startDate' => '06/15/2020',
        'endDate' => '06/21/2020'
    ];

    /**
     * @test
     * service/symbols [GET]
     */
    public function testUrlShouldExist(): void
    {
        $this->get('service/symbols')->seeStatusCode(200);
    }

    /**
     * @test
     */
    public function testShouldReturnSymbolsArray(): void
    {
        $mock = Mockery::mock(ServiceController::class)
            ->makePartial()
            ->shouldReceive('symbols')
            ->once()
            ->andReturn(new Response(File::get(base_path() . '/tests/stubs/responses/service-controller.json')));

        $this->app->instance(ServiceController::class, $mock->getMock());
        $this->get('service/symbols')
            ->seeJsonEquals([0 => "AAIT", 1 => "AAL", 2 => "AAME"]);
    }

    /**
     * @test
     * service/submit [POST]
     */
    public function testShouldCallRapidApi(): void
    {
        $this->post('service/submit', $this->sendData)->seeJsonStructure(['*' => [
            'date', 'open', 'close', 'high', 'low', 'volume'
        ]]);
    }

    /**
     * @test
     */
    public function testMailShouldBeSent(): void
    {
        Mail::fake();
        Mail::assertNothingSent();

        $this->post('service/submit', $this->sendData);

        Mail::assertSent(function (ServiceEmail $mail)  {
            return $mail->hasTo($this->sendData['email']);
        });
    }
}
