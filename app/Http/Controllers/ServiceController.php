<?php

namespace App\Http\Controllers;

use App\Mail\ServiceEmail;
use App\Service;
use Exception;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class ServiceController extends Controller
{
    /**
     * @return Response|JsonResponse
     */
    public function symbols()
    {
        try {
            $response = Http::get(Service::SYMBOLS_URL);
            if ($response->ok()) {
                $symbolsArray = array_column($response->json(), 'Symbol');
                return response()->json(array_unique($symbolsArray), 200);
            }
            return $response->throw();

        } catch (Exception $e) {
            Log::error($e->getMessage());
            return response()->json($e->getMessage(), 404);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws RequestException
     */
    public function store(Request $request)
    {
        try {

            $this->validate($request, [
                'companySymbol' => 'required',
                'email' => 'required|email',
                'startDate' => 'required|date|before_or_equal:endDate|before_or_equal:today',
                'endDate' => 'required|date|after_or_equal:startDate|after_or_equal:today',
            ]);

            try {
                $response = Http::get(Service::SYMBOLS_URL);
                if ($response->ok()) {
                    $companiesArray = array_column($response->json(), 'Company Name', 'Symbol');
                    $companyName = $companiesArray[$request->input('companySymbol')];
                    Mail::to($request->input('email'))->send(new ServiceEmail([
                        'subject' => "Company’s Name = {$companyName}",
                        'message' => "From {$request->input('startDate')} to {$request->input('endDate')}"
                    ]));
                }

            } catch (Exception $e) {
                Log::error($e->getMessage());
            }


            return response()->json((new Service)->callRapidApi(
                $request->input('startDate'),
                $request->input('endDate'),
                $request->input('companySymbol'))
            ['prices']
            );
        } catch (ValidationException $e) {
            Log::error($e->getMessage());
            return response()->json($e, 422);
        }
    }


}
