<?php


namespace App;

use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class Service
{
    const SYMBOLS_URL = 'https://pkgstore.datahub.io/core/nasdaq-listings/nasdaq-listed_json/data/a5bc7580d6176d60ac0b2142ca8d7df6/nasdaq-listed_json.json';
    const RAPID = [
        'url' => 'https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-historical-data',
        'X-RapidAPI-Host' => 'apidojo-yahoo-finance-v1.p.rapidapi.com',
    ];

    /**
     * @param string $period1
     * @param string $period2
     * @param string $symbol
     * @return array|Response|mixed
     * @throws RequestException
     */
    public function callRapidApi(string $period1, string $period2, string $symbol)
    {
        $response = Http::withHeaders([
            'x-rapidapi-host' => self::RAPID['X-RapidAPI-Host'],
            'x-rapidapi-key' => env('RAPID_TOKEN'),
        ])->get(self::RAPID['url'], [
            'frequency' => '1d',
            'filter' => 'history',
            'period1' => strtotime($period1),
            'period2' => strtotime($period2),
            'symbol' => $symbol,
        ]);

        if ($response->ok()) {
            return $response->json();
        }
        return $response->throw();
    }
}
