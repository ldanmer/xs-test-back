# Xs Test Backend
This project was build with Laravel Lumen 7.1

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## How to set up
### Requirements
- PHP >= 7.2
- OpenSSL PHP Extension
- Mbstring PHP Extension

### Build
- Run composer: 
`php composer install`
- Set up config: `.env` (see `.env.example`)
- Run local server `composer run-script start`
- Run tests: `composer run-script test`
